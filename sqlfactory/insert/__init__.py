"""INSERT statement builder."""

from sqlfactory.insert.insert import INSERT, Insert
from sqlfactory.insert.values import Values

__all__ = ["INSERT", "Insert", "Values"]
