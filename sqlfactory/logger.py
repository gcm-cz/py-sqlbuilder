"""Module that provides named logger for SQL Builder."""

import logging

logger = logging.getLogger("sqlfactory")
