"""UPDATE statement builder."""

from sqlfactory.update.update import UPDATE, Update, UpdateColumn

__all__ = ["UPDATE", "Update", "UpdateColumn"]
