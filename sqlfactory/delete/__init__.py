"""DELETE statement builder."""

from sqlfactory.delete.delete import DELETE, Delete

__all__ = ["DELETE", "Delete"]
